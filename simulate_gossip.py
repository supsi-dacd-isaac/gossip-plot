import networkx as nx
import numpy as np
from tqdm import tqdm
import random
import matplotlib.pyplot as plt

import matplotlib
matplotlib.use('Qt5Agg')


def random_conn_graph(n, p):

    full_edges = n * (n - 1) / 2
    gr = nx.complete_graph(n)
    assert nx.is_connected(gr)

    while True:
        n1, n2 = random.choice(list(gr.edges))
        gr.remove_edge(n1, n2)

        if not nx.is_connected(gr):
            gr.add_edge(n1, n2)
            continue

        if gr.number_of_edges() < p*full_edges:
            break

    return gr


def random_value_map(_gr, _n):
    return np.random.rand()


def do_gossip(graph, value_fn, rtol=1e-4, bar=False, plot=False):

    if plot:
        pos = nx.spring_layout(graph)
        plt.ion()

    for x in graph.nodes:
        graph.nodes[x]['value'] = value_fn(graph, x)

    it = 0
    bar = tqdm(disable=not bar)
    while True:

        node = np.random.choice(graph.nodes)
        targo = np.random.choice(list(graph.neighbors(node)))

        it += 1
        bar.update()

        new_value = np.mean([graph.nodes[node]['value'], graph.nodes[targo]['value']])
        graph.nodes[node]['value'] = new_value
        graph.nodes[targo]['value'] = new_value

        vls = np.asarray(list(nx.get_node_attributes(graph, 'value').values()))
        if np.allclose(vls, np.mean(vls), rtol=rtol):
            break

        if plot:
            plt.clf()
            plt.title(it)
            nx.draw_networkx(graph, pos, node_color=vls, vmin=0, vmax=1, cmap='coolwarm')
            nx.draw_networkx_edges(graph, pos, edgelist=[(node, targo)], width=2.0, edge_color='r')
            plt.draw()
            plt.pause(.001)

    return it


if __name__ == '__main__':

    n = 18
    p = 0.2
    rtol = 1e-2

    graph = random_conn_graph(n, p)
    for attempt in range(10):
        print(do_gossip(graph, random_value_map, rtol=rtol, bar=False, plot=True))